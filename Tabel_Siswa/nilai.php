<!DOCTYPE html>
<html>
<head>
	<title>Daftar Siswa dan Nilai</title>
</head>
<body>
    <tr>
        <td><a href="/ZKI/welcome.php"><button>Kembali</a></button></td>
    </tr>
        
    <form method="GET">
        <h1>Daftar Siswa dan Nilai</h1>
    <table border="1">
        <tr>
            <th bgcolor="aqua">SISWA</th>
            <th bgcolor="yellow">MTK</th>
            <th bgcolor="yellow">B.INDO</th>
            <th bgcolor="yellow">PPKN</th>
        </tr>

    <?php
        $siswa = array(
        "Ilyas" => array("MTK" => "10", "B.INDO" => "8", "PPKN" => "7"),
        "Arif" => array("MTK" => "9", "B.INDO" => "10", "PPKN" => "8"),
        "Ijul" => array("MTK" => "10", "B.INDO" => "10", "PPKN" => "5"),
    );
    
    foreach ($siswa as $S => $nilai) {
        echo "<tr>";
        echo "<td>".$S."</td>";
        if($nilai["MTK"] < 6){
            echo "<td style='color:red'>".$nilai["MTK"]."</td>";
        } else {
            echo "<td>" .$nilai["MTK"]."</td>";
        }
        if($nilai["B.INDO"] < 6){
            echo "<td style='color:red'>".$nilai["B.INDO"]."</td>";
        } else {
        echo "<td>".$nilai["B.INDO"]."</td>";
        }
        if($nilai["PPKN"] < 6){
            echo "<td style='color:red'>".$nilai["PPKN"]."</td>";
        } else {
        echo "<td>".$nilai["PPKN"]."</td>";
        }
        echo "</tr>";
    }
    ?>

</table>
</form>
</body>
</html>