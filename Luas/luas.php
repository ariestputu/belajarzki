<!DOCTYPE html>  
<html>
    <head>
        <title>Konversi Satuan Panjang</title>     

<?php
ini_set('display_errors',0);
 
if( isset( $_POST['submit'] ))
{
$sat1=$_POST['satuan1'];
$sat2=$_POST['satuan2'];
$n=$_POST['nilai'];
$konversi = array(
    "Kilometer" => array("Kilometer"=>1,"Hektometer"=>10,"Dekameter"=>100,"Meter"=>1000,"Desimeter"=>10000,"Centimeter"=>100000,"Milimeter"=>1000000),
    "Hektometer" => array("Kilometer"=>0.1,"Hektometer"=>1,"Dekameter"=>10,"Meter"=>100,"Desimeter"=>1000,"Centimeter"=>10000,"Milimeter"=>100000),
    "Dekameter" => array("Kilometer"=>0.01,"Hektometer"=>0.1,"Dekameter"=>1,"Meter"=>10,"Desimeter"=>100,"Centimeter"=>1000,"Milimeter"=>10000),
    "Meter" => array("Kilometer"=>0.001,"Hektometer"=>0.01,"Dekameter"=>0.1,"Meter"=>1,"Desimeter"=>10,"Centimeter"=>100,"Milimeter"=>1000),
    "Desimeter" => array("Kilometer"=>0.0001,"Hektometer"=>0.001,"Dekameter"=>0.01,"Meter"=>0.1,"Desimeter"=>1,"Centimeter"=>10,"Milimeter"=>100),
    "Centimeter" => array("Kilometer"=>0.00001,"Hektometer"=>0.0001,"Dekameter"=>0.001,"Meter"=>0.01,"Desimeter"=>0.1,"Centimeter"=>1,"Milimeter"=>10),
    "Milimeter" => array("Kilometer"=>0.000001,"Hektometer"=>0.00001,"Dekameter"=>0.0001,"Meter"=>0.001,"Desimeter"=>0.01,"Centimeter"=>0.1,"Milimeter"=>1)
    );
    $res=$n*$konversi[$sat1][$sat2];
}
?>
</head>
<body>
    <tr>
        <td><a href="/ZKI/welcome.php"><button>Kembali</a></button></td>
    </tr>
    
    <form method="POST" action=""> 
        <h1>Konversi Satuan Panjang PHP</h1>  
    <table>
        <tr>
        <td><input type="number" name="nilai"></td>
            <td><select name="satuan1">
                <option value="Kilometer"selected>Kilometer</option>
                <option value="Hektometer">Hektometer</option>
                <option value="Dekameter">Dekameter</option>
                <option value="Meter">Meter</option>
                <option value="Desimeter">Desimeter</option>
                <option value="Centimeter">Centimeter</option>
                <option value="Milimeter">Milimeter</option>
            </select>
        </td>
        <td><b>>>>&nbsp</b><select name="satuan2">
                <option value="Kilometer">Kilometer</option>
                <option value="Hektometer">Hektometer</option>
                <option value="Dekameter">Dekameter</option>
                <option value="Meter" selected>Meter</option>
                <option value="Desimeter">Desimeter</option>
                <option value="Centimeter">Centimeter</option>
                <option value="Milimeter">Milimeter</option>
            </select>
        </td>
    </tr>

    <tr>
        <td><input type="submit" name="submit" value="Konversi">&nbsp<a href="luas.php">Reset</td>
    </tr>

    <tr>
        <td>Hasil Konversi:<?php echo $res; '<script>alert("In'?></td>
    </tr>

</table>
</form>
</body>
</html>