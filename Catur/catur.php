<!DOCTYPE html>
<html>
<head>
	<title>Tabel Pola Catur</title>
	<style>
    	.catur {
      		margin: 10px;
    	}
    	.catur td {
      		width: 50px;
      		height: 50px;
      		border: 1px solid black;
    	}
    	.catur .hitam {
      		background-color: black;
    	}
    	.catur .putih {
      		background-color: white;
    	}
  	</style>
</head>
<body>
	<tr>
        <td><a href="/ZKI/welcome.php"><button>Kembali</a></button></td>
    </tr>
	
	<form method="POST" action="">
		<h1>Buat Papan Catur</h1>
		
		<label>Baris:</label>
		<input type="number" name="baris">
		<label>Kolom:</label>
		<input type="number" name="kolom">
		<select name="jumlah">
			<option value="ganjil">Ganjil</option>
			<option value="genap">Genap</option>

		<td><input type="submit" value="Buat Papan"></td>
		<td>&nbsp<a href="catur.php">Reset</td>
	</form>

	<?php
	ini_set('display_errors',0);

	if (isset($_POST['baris']) && isset($_POST['kolom'])) {
		$baris = $_POST['baris'];
		$kolom = $_POST['kolom'];
		$jumlah = $_POST['jumlah'];

		echo '<table class="catur">';
		$no=1;
    	for ($i = 1; $i <= $baris; $i++) {
			echo '<tr>';
			for ($j = 1; $j <= $kolom; $j++) {
				//$class=($jumlah == 'genap') ? ($no %2 == 0? 'hitam' : 'putih'):($no %2 == 1? 'hitam' : 'putih');
				$class= null;
				if($jumlah == 'genap'){
					if($no %2 == 0){
						$class= 'hitam';
					}
					else{
						$class= 'putih';
					}
				} else{
					if($no %2 == 1){
						$class= 'hitam';
					}
					else{
						$class= 'putih';
					}
				}
				echo '<td class="'.$class.'">'.$no.'</td>';
				$no++;
			}
			echo '</tr>';
		}
		echo '</table>';
	}
	?>
</body>
</html>
